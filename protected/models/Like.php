<?php
class Like {
	function __construct($data){
		$this->rawData = $data;
		$this->id = $data["id"];
		$this->name = $data["name"];
	}
	private $id, $name, $rawData;

	public function __set($key,$value){
		$this->$key = $value;
	}

	public function __get($key){
		return $this->$key;
	}
}
?>

