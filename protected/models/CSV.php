<?php
class CSV {
	function __construct($data){
		$this->postDataAry = $data;
	}

	private $postDataAry;
	public $fileName = "csv/Post_from_feed.csv";

	public function __set($key,$value){
		$this->$key = $value;
	}
	public function __get($key){
		return $this->$key;
	}
	public function getCSV(){
		$postCsvData[] = $this->postColumnHeader;
		$likeCsvData[] = $this->likeColumnHeader;
		foreach($this->postDataAry as $postData){
			$postCsvData[] = $this->getPostAryData($postData);
			foreach($postData->likeDetails as $like){
				$likeCsvData[] = $this->getLikeAryData($postData->page_id, $postData->post_id, $like);
			}
		}
		$file = fopen($this->fileName, "w");
		if($file){
			foreach($postCsvData as $row) fputcsv($file, $row);
			foreach($likeCsvData as $row) fputcsv($file, $row);
		}
		fclose($file);
	}

	private function getOrElse($data,$keys,$else = ""){
		foreach($keys as $key){
			$data = isset($data[$key]) ? $data[$key] : $else;
			if($data == $else) break;
		}
		return $data;
	}

	private function getPostAryData($postData){
		return array($postData->page_id,
			$postData->post_id,
			$this->getOrElse($postData->rawData, array("from", "name")),
			$this->getOrElse($postData->rawData, array("from", "category")),
			$this->getOrElse($postData->rawData, array("from", "id")),
			$postData->ownerPost,
			$this->getOrElse($postData->rawData, array("to", "data", 0, "name")),
			$this->getOrElse($postData->rawData, array("to", "data", 0, "category")),
			$this->getOrElse($postData->rawData, array("to", "data", 0, "id")),
			$this->getOrElse($postData->rawData, array("message")),
			$postData->messageTags ? "True":"False",
			$this->getOrElse($postData->rawData, array("picture")),
			$this->getOrElse($postData->rawData, array("link")),
			$this->getOrElse($postData->rawData, array("name")),
			$this->getOrElse($postData->rawData, array("caption")),
			$this->getOrElse($postData->rawData, array("description")),
			$this->getOrElse($postData->rawData, array("source")),
			$this->getOrElse($postData->rawData, array("properties",0,"text")),
			$this->getOrElse($postData->rawData, array("icon")),
			$this->getOrElse($postData->rawData, array("actions", 0, "name")),
			$this->getOrElse($postData->rawData, array("actions", 0, "link")),
			$this->getOrElse($postData->rawData, array("actions", 1, "name")),
			$this->getOrElse($postData->rawData, array("actions", 1, "link")),
			$this->getOrElse($postData->rawData, array("privacy", "description")),
			$this->getOrElse($postData->rawData, array("privacy", "value")),
			$this->getOrElse($postData->rawData, array("type")),
			$postData->likes,
			$this->getOrElse($postData->rawData, array("place", "name")),
			$this->getOrElse($postData->rawData, array("story")),
			$postData->storyTags,
			$postData->withTags,
			$postData->comments,
			$this->getOrElse($postData->rawData, array("object_id")),
			$this->getOrElse($postData->rawData, array("application", "name")),
			$this->getOrElse($postData->rawData, array("application", "id")),
			$this->getOrElse($postData->rawData, array("created_time")),
			$this->getOrElse($postData->rawData, array("updated_time")),
			date('c'));
	}

	private function getLikeAryData($page_id, $post_id, $like){
		return array($page_id,
			$post_id,
			$like->name,
			$this->getOrElse($like->rawData, array("category")),
			$like->id,
			date('c')
		);
	}
	private $likeColumnHeader = array('page_id',
		'post_id',
		'individual_name',
		'individual_category',
		'individual_id',
		'data_aquired_time'
	);

	private $postColumnHeader = array('page_id',
			'post_id',
			'from: name',
			'from: category',
			'from: id',
			'Page_Owner',
			'to: name',
			'to: category',
			'to: id',
			'message',
			'message_tags',
			'picture',
			'link',
			'name',
			'caption',
			'description',
			'source',
			'properties',
			'icon',
			'actions: name:(Comment)',
			'action: link (Comment)',
			'actions: name: (Like)',
			'action: link (Like)',
			'privacy: description',
			'privacy: value',
			'type',
			'likes',
			'place',
			'story',
			'story_tags',
			'with_tags',
			'comments',
			'object_id',
			'application: name',
			'application: id',
			'created_time',
			'updated_time',
			'data_aquired_time');

}
?>
