<?php
class PostData {
	function __construct($data){
		$uid = split("_", $data["id"]);
		$this->rawData = $data;
		$this->page_id = $uid[0];
		$this->post_id = $uid[1];
		$this->ownerPost = $this->page_id == $data["from"]["id"] ? 1 : 0;
		if(isset($data["likes"])) $this->likes = count($data["likes"]["data"]);
		if(isset($data["comments"])) $this->comments = count($data["comments"]);
		if(isset($data["message_tags"])) $this->messageTags = $data["message_tags"] ? 1 : 0;
		if(isset($data["story_tags"])) $this->storyTags = $data["story_tags"] ? 1 : 0;
		if(isset($data["with_tags"])) $this->withTags = $data["with_tags"] ? 1 : 0;
	}

	private $rawData, $page_id, $post_id, $likeDetails = Array();
	private $ownerPost,$likes,$comments,$messageTags,$storyTags,$withTags;

	public function __set($key,$value){
		$this->$key = $value;
	}
	public function __get($key){
		return $this->$key;
	}
}
?>
