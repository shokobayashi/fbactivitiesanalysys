<?php 
$file = Yii::app()->basePath.'/../csv/Post_from_feed.csv';
if ( !file_exists($file) ) {
  echo 'File('.$file.') does not exist. Please try again.';
}else{
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="Post_from_feed.csv"');
	header('Content-Length: '.filesize($file));
	readfile($file);
}
?>

